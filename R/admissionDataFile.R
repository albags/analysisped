#' Admission data file format for comoRbidity analysis
#'
#' Given a file containing admission data, it will be process and given a new format for
#' further processing.
#'
#' @param path Path where the files are located
#' @param fileName The name of the file that contains the admission data.
#' @param code The code you want to be added to the patient identifier according to the hospital
#' @export admissionDataFile



admissionDataFile <- function( path, fileName, code ){

  message( "Loading the file")
  input <- read.delim(paste0(path, fileName), header = TRUE, colClasses = "character", quote = "")


  # message("Checking the input file")
  # checkColnames <- c("IPP","NOM","PRENOM","DATENAISS","SEXE","ADR3","CPOST","CODEM","MED" )
  # inputColnames <- colnames( input )
  #
  # if(! checkColnames %in% inputColnames != TRUE){
  #   message( "Te input file format is not correct.\nPlease revise it and try it again")
  #   stop()
  # }

  message( "Changing date format")
  input$IPP <- sapply(strsplit(as.character(input$IPPDATE), "-"), `[`, 1)
  input$admission_id <- sapply(strsplit(as.character(input$IPPDATE), "-"), `[`, 2)
  input$admissionStartDate <- paste(sapply(strsplit(as.character(input$DATEJ), "/"), `[`, 3),
                                    sapply(strsplit(as.character(input$DATEJ), "/"), `[`, 2),
                                    sapply(strsplit(as.character(input$DATEJ), "/"), `[`, 1),
                                    sep = "-")
  input$admissionEndDate <- paste(sapply(strsplit(as.character(input$DATESORT), "/"), `[`, 3),
                                  sapply(strsplit(as.character(input$DATESORT), "/"), `[`, 2),
                                  sapply(strsplit(as.character(input$DATESORT), "/"), `[`, 1),
                                  sep = "-")

  checkStartDate <- as.data.frame(summary(as.factor(nchar(input$admissionStartDate))))
  checkEndDate   <- as.data.frame(summary(as.factor(nchar(input$admissionEndDate))))

  if(nrow( checkStartDate ) != 1 & rownames( checkStartDate)[1] != 10){


    admissionStartDate <- input[ nchar(input$admissionStartDate) != 10, ]
    message("There are ", nrow(admissionStartDate), " patients with admission start date error")
    write.table( admissionStartDate, file= paste0(path, "/patientsErrorAdmissionDate.txt"),
                 col.names = TRUE, row.names = FALSE, sep = "\t", quote = FALSE)
  }


  message( "Selecting the desired columns")
  input <- input[c("IPP", "admission_id", "admissionStartDate", "admissionEndDate")]

  message( "Mapping patients")
  mappingFile <- read.delim(paste0(path, "/mappingFilepatientData", code, ".txt"), header = TRUE, colClasses = "character", quote = "")

  output <- input[ input$IPP %in% mappingFile$IPP, ]
  output <- merge( mappingFile, output, by="IPP")
  output <- output[c("patient_id", "admission_id", "admissionStartDate", "admissionEndDate")]

  message( "For the ", length(unique(mappingFile$patient_id)), " patients presents in the patient file,\nthere is admission information for ", length(unique(output$patient_id)), " of them.")

  #write.table(output, file=paste0(path, "/admissionDataPED.txt"), col.names = TRUE, row.names = FALSE, sep = "\t", quote = FALSE)

  return( output )

}
